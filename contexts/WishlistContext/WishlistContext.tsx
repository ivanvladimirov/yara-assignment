'use client'
import { WishlistAction, WishlistContextType, WishlistFunctions, WishlistState } from '@/types/wishlist'
import { createContext, useReducer, useContext, useEffect, ReactNode, useCallback } from 'react'

const wishlistReducer = (state: WishlistState, action: WishlistAction) => {
  const { type, payload } = action

  switch (type) {
    case 'LOAD_WISHLIST':
      return {
        ...state,
        wishlist: payload.wishlist,
        loadingWishlist: payload.loadingWishlist
      }
    case 'UPDATE_WISHLIST':
      localStorage.setItem('wishlist', JSON.stringify(payload.wishlist))
      return {
        ...state,
        wishlist: payload.wishlist
      }
    default:
      return state
  }
}

const initialState = {
  wishlist: [],
  loadingWishlist: true,
  updateWishlist: () => {}
}

const WishlistContext = createContext<WishlistContextType>(initialState)

const WishlistProvider = ({ children }: { children: ReactNode }) => {
  const [state, dispatch] = useReducer(wishlistReducer, initialState)

  useEffect(() => {
    if (typeof window !== 'undefined') {
      dispatch({
        type: 'LOAD_WISHLIST',
        payload: {
          wishlist:
            localStorage && !!localStorage.getItem('wishlist')
              ? JSON.parse(localStorage.getItem('wishlist') as string) ?? []
              : [],
          loadingWishlist: false
        }
      })
    }
  }, [])

  const updateWishlist: WishlistFunctions['updateWishlist'] = useCallback(
    (eventId) => {
      const wishlist = state.wishlist

      if (wishlist.includes(eventId)) {
        dispatch({
          type: 'UPDATE_WISHLIST',
          payload: {
            wishlist: wishlist.filter((item) => item !== eventId)
          }
        })
      } else {
        dispatch({
          type: 'UPDATE_WISHLIST',
          payload: {
            wishlist: [...wishlist, eventId]
          }
        })
      }
    },
    [state.wishlist]
  )

  return <WishlistContext.Provider value={{ ...state, updateWishlist }}>{children}</WishlistContext.Provider>
}

const useWishlist = () => {
  const context = useContext(WishlistContext)

  if (context === undefined) {
    throw new Error('useWishlist can only be used inside WishlistProvider')
  }

  return context
}

export { WishlistProvider, useWishlist }
