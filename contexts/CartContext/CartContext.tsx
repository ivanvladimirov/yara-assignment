'use client'
import { MAX_TICKETS_TO_BUY } from '@/app-config'
import { CartAction, CartContextType, CartFunctions, CartState } from '@/types/cart'
import { useTranslations } from 'next-intl'
import { enqueueSnackbar } from 'notistack'
import { createContext, useReducer, useContext, ReactNode, useCallback, useEffect, useState } from 'react'

const cartReducer = (state: CartState, action: CartAction) => {
  const { type, payload } = action

  switch (type) {
    case 'LOAD_CART':
      return {
        ...state,
        cart: payload.cart,
        loadingCart: payload.loadingCart
      }
    case 'UPDATE_CART':
      localStorage.setItem('cart', JSON.stringify(payload.cart))
      return {
        ...state,
        cart: payload.cart
      }

    default:
      return state
  }
}

const initialState = {
  cart: [],
  total: 0,
  loadingCart: true,
  openCart: false,
  setOpenCart: () => {},
  addToCart: () => {},
  removeFromCart: () => {},
  itemQuantity: () => 0
}

const CartContext = createContext<CartContextType>(initialState)

const CartProvider = ({ children }: { children: ReactNode }) => {
  const t = useTranslations('Cart')
  const [state, dispatch] = useReducer(cartReducer, initialState)
  const [openCart, setOpenCart] = useState(false)

  useEffect(() => {
    if (typeof window !== 'undefined') {
      dispatch({
        type: 'LOAD_CART',
        payload: {
          cart:
            localStorage && !!localStorage.getItem('cart')
              ? JSON.parse(localStorage.getItem('cart') as string) ?? []
              : [],
          loadingCart: false
        }
      })
    }
  }, [])

  const itemQuantity: CartFunctions['itemQuantity'] = useCallback(
    (eventId) => {
      const cartItem = state.cart.find((item) => item.id === eventId)
      return !!cartItem ? cartItem.quantity : 0
    },
    [state.cart]
  )

  const addToCart: CartFunctions['addToCart'] = useCallback(
    (event) => {
      const cart = state.cart
      const isNewItem = !cart.map((item) => item.id).includes(event.id)

      if (isNewItem) {
        dispatch({
          type: 'UPDATE_CART',
          payload: { cart: [...cart, event] }
        })

        enqueueSnackbar(t('TicketsAdded'))
      } else {
        if (itemQuantity(event.id) && event.quantity + itemQuantity(event.id) >= MAX_TICKETS_TO_BUY) {
          dispatch({
            type: 'UPDATE_CART',
            payload: {
              cart: cart.map((item) =>
                item.id === event.id
                  ? {
                      ...item,
                      quantity: MAX_TICKETS_TO_BUY
                    }
                  : item
              )
            }
          })

          enqueueSnackbar(
            t('OnlyXItemsAdded', { count: MAX_TICKETS_TO_BUY - itemQuantity(event.id), max: MAX_TICKETS_TO_BUY }),
            { variant: 'info' }
          )
        } else {
          dispatch({
            type: 'UPDATE_CART',
            payload: {
              cart: cart.map((item) =>
                item.id === event.id
                  ? {
                      ...item,
                      quantity: item.quantity + event.quantity
                    }
                  : item
              )
            }
          })

          enqueueSnackbar(t('TicketsAdded'))
        }
      }
    },
    [itemQuantity, state.cart, t]
  )

  const removeFromCart: CartFunctions['removeFromCart'] = useCallback(
    (eventId) => {
      const cartItem = state.cart.find((item) => item.id === eventId)
      const cart = state.cart

      if (!!cartItem) {
        if (cartItem.quantity <= 1) {
          dispatch({
            type: 'UPDATE_CART',
            payload: { cart: cart.filter((item) => item.id !== eventId) }
          })
        } else {
          dispatch({
            type: 'UPDATE_CART',
            payload: {
              cart: cart.map((item) =>
                item.id === eventId
                  ? {
                      ...item,
                      quantity: item.quantity - 1
                    }
                  : item
              )
            }
          })
        }

        enqueueSnackbar(t('TicketRemoved'))
      }
    },
    [state.cart, t]
  )

  return (
    <CartContext.Provider
      value={{
        ...state,
        openCart,
        total: state.cart.reduce((acc, value) => {
          return value.price * value.quantity + acc
        }, 0),
        addToCart,
        setOpenCart,
        itemQuantity,
        removeFromCart
      }}
    >
      {children}
    </CartContext.Provider>
  )
}

const useCart = () => {
  const context = useContext(CartContext)

  if (context === undefined) {
    throw new Error('useCart can only be used inside CartProvider')
  }

  return context
}

export { CartProvider, useCart }
