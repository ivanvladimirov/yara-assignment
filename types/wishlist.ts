import { Event } from './event'

export type EventId = Event['id']

export type WishlistState = {
  wishlist: EventId[]
  loadingWishlist: boolean
}

export type WishlistFunctions = {
  updateWishlist: (eventId: EventId) => void
}

export type WishlistContextType = WishlistFunctions & WishlistState

export type WishlistAction =
  | {
      type: 'LOAD_WISHLIST'
      payload: {
        wishlist: WishlistState['wishlist']
        loadingWishlist: WishlistState['loadingWishlist']
      }
    }
  | {
      type: 'UPDATE_WISHLIST'
      payload: {
        wishlist: WishlistState['wishlist']
      }
    }
