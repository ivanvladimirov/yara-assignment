import { Dispatch, SetStateAction } from 'react'
import { Event } from './event'

export type CartItem = {
  id: Event['id']
  name: Event['short_title']
  quantity: number
  price: number
}

export type CartState = {
  cart: CartItem[]
  total: number
  loadingCart: boolean
}

export type CartFunctions = {
  addToCart: (event: CartItem) => void
  itemQuantity: (eventId: Event['id']) => number
  removeFromCart: (eventId: Event['id']) => void
}

export type CartContextType = CartFunctions &
  CartState & {
    openCart: boolean
    setOpenCart: Dispatch<SetStateAction<boolean>>
  }

export type CartAction =
  | {
      type: 'LOAD_CART'
      payload: {
        cart: CartState['cart']
        loadingCart: CartState['loadingCart']
      }
    }
  | {
      type: 'UPDATE_CART'
      payload: {
        cart: CartState['cart']
      }
    }
