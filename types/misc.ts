export type KeyJoin<A, B> = `${string & keyof A}.${string & B}`
