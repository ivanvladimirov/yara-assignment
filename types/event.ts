import { KeyJoin } from './misc'

export type Taxonomy = {
  parent_id: number | null
  id: number
  name: string
}

export type Stats = {
  listing_count: number
  average_price: number
  lowest_price: number
  highest_price: number
}

export type Performer = {
  name: string
  short_name: string
  url: string
  image: string | null
  images: {
    large: string
    huge: string
    small: string
    medium: string
  } | null
  primary: boolean
  id: number
  score: number
  type: string
  slug: string
  taxonomies: Taxonomy[]
}

export type Venue = {
  city: string
  name: string
  extended_address: string | null
  url: string
  country: string
  state: string
  score: number
  postal_code: string
  location: {
    lat: number
    lon: number
  }
  address: string | null
  id: number
}

export type Event = {
  stats: Stats
  title: string
  url: string
  datetime_local: string
  performers: Performer[]
  venue: Venue
  short_title: string
  datetime_utc: string
  score: number
  taxonomies: Taxonomy[]
  type: string
  id: number
}

export type Meta = { total: number; took: number; page: number; per_page: number; geolocation: any }

export interface FetchEventsResponse {
  events: Event[]
  meta: Meta
}

type EventQueryParamsWithKey<Key extends string> = {
  [k in Key]: number | string
}

type FilterOperators = 'gt' | 'gte' | 'lt' | 'lte' | 'eq'
type FilterOptions = KeyJoin<Event['stats'] & { score: null }, FilterOperators>

type SortOperators = 'asc' | 'desc'
type SortingOptions = KeyJoin<Pick<Event, 'datetime_local' | 'datetime_utc' | 'id' | 'score'>, SortOperators>

type TaxonomyOperators = keyof Taxonomy
type TaxonomyOptions = KeyJoin<{ taxonomies: null }, TaxonomyOperators>

type PerformerOperators = 'id' | 'slug'
type PerformerOptions = KeyJoin<{ performers: null }, PerformerOperators>

type VenueOperators = 'id' | 'city' | 'state'
type VenueOptions = KeyJoin<{ venue: null }, VenueOperators>

export type EventQueryParams = {
  per_page: string
  page: string
  datetime_utc: Event['datetime_utc']
  q: string
  id: string | string[]
} & EventQueryParamsWithKey<FilterOptions> &
  EventQueryParamsWithKey<SortingOptions> &
  EventQueryParamsWithKey<TaxonomyOptions> &
  EventQueryParamsWithKey<SortingOptions> &
  EventQueryParamsWithKey<PerformerOptions> &
  EventQueryParamsWithKey<VenueOptions>

export type CreateEventValuesType = {
  short_title: Event['short_title']
  stats: {
    lowest_price: string
  }
  datetime_utc: Event['datetime_utc']
  venue: {
    city: Venue['city']
    state: Venue['state']
    address: Venue['address']
    extended_address: Venue['extended_address']
    name: Venue['name']
  }
  performers: {
    name: Performer['name']
    image: Performer['image']
    taxonomies: {
      id: Taxonomy['id']
      name: Taxonomy['name']
    }[]
  }[]
  taxonomies: string[]
}
