/** @type {import('next').NextConfig} */
const withNextIntl = require('next-intl/plugin')('./translations/i18n.ts')

const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'seatgeek.com',
        port: '',
        pathname: '/images/**'
      }
    ]
  }
}
module.exports = withNextIntl(nextConfig)
