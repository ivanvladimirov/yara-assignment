import { Container, Typography } from '@mui/material'

export default function NotFound() {
  return (
    <Container sx={{ pt: 4 }}>
      <Typography variant='h6' align='center'>
        404
      </Typography>
    </Container>
  )
}
