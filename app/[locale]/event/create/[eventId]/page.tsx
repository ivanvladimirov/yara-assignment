'use client'
import { Details } from '@/components/Event'
import { GetTickets } from '@/components/Event/Heading/components'
import type { Event, Performer as PerformerType } from '@/types/event'
import { DUMMY_TEXTS } from '@/utils/dummyTexts'
import { Box, CircularProgress, Container, Grid, Paper, Stack, Typography } from '@mui/material'
import { useTranslations } from 'next-intl'
import Image from 'next/image'
import { notFound, useParams } from 'next/navigation'
import { useEffect, useMemo, useState } from 'react'

export default function Page() {
  const t = useTranslations()
  const params = useParams()
  const eventId = useMemo(() => params.eventId, [params.eventId])

  const [event, setEvent] = useState<Event | null>(null)
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const eventsData = !!localStorage.getItem('my-events')
        ? JSON.parse(localStorage.getItem('my-events') as string)
        : []
      setEvent(eventsData.find((item: Event) => item.id === +eventId))
      setIsLoading(false)
    }
  }, [eventId])

  if (isLoading)
    return (
      <Container sx={{ textAlign: 'center', pt: 4 }}>
        <CircularProgress />
      </Container>
    )

  if (!isLoading && !event) {
    notFound()
  }

  return (
    <Stack>
      <Box
        sx={{
          background:
            'linear-gradient(to bottom, rgba(22, 28, 36, 0.8), rgba(22, 28, 36, 0.8)),url(/images/homepage_gradient.jpg)',
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          color: 'common.white'
        }}
      >
        <Container
          sx={{
            minHeight: 300,
            display: 'flex',
            flexDirection: { xs: 'column', md: 'row' },
            alignItems: 'center',
            justifyContent: { xs: 'center', md: 'space-between' }
          }}
        >
          <Stack mb={{ xs: 2, md: 0 }}>
            <Typography variant='h3' sx={{ textAlign: { xs: 'center', md: 'left' } }}>
              {event?.short_title}
            </Typography>
            <Typography
              sx={{ opacity: 0.5, display: 'flex', justifyContent: { xs: 'center', md: 'left' } }}
              component='div'
            >
              {event?.taxonomies.map((taxonomy) => (
                <Box mr={3} key={taxonomy.id} sx={{ '&:last-of-type': { mr: 0 } }}>
                  {t(`Taxonomies.${taxonomy.name}`)}
                </Box>
              ))}
            </Typography>
          </Stack>
          <GetTickets event={event as Event} />
        </Container>
      </Box>
      <Container sx={{ mt: 8 }}>
        <Grid container spacing={{ xs: 5, md: 8 }} flexDirection={{ xs: 'column-reverse', md: 'row' }}>
          <Grid item xs={12} md={7} lg={8}>
            <Stack mb={4}>
              <Typography variant='h5' gutterBottom>
                {t('Event.Description')}
              </Typography>
              <Typography>{DUMMY_TEXTS.EVENT_DESCRIPTION}</Typography>
            </Stack>
            <Stack mb={4}>
              <Typography variant='h5' gutterBottom>
                {t('Event.AboutThePerformers')}
              </Typography>
              <Grid container spacing={2}>
                {event?.performers.map((performer: PerformerType) => (
                  <Grid item xs={12} sm={6} key={performer.id}>
                    <Paper
                      sx={{
                        display: 'flex',
                        alignItems: 'center',
                        border: '1px solid rgba(145, 158, 171, 0.16)',
                        p: 2
                      }}
                    >
                      <Box
                        sx={{
                          position: 'relative',
                          minWidth: 64,
                          minHeight: 64,
                          borderRadius: 2.5,
                          overflow: 'hidden'
                        }}
                      >
                        {performer?.image && (
                          <Image
                            fill
                            alt={performer?.name}
                            src={performer?.image as string}
                            style={{ objectFit: 'cover' }}
                          />
                        )}
                      </Box>
                      <Stack ml={1.5} overflow='hidden'>
                        <Typography variant='h6' noWrap title={performer.name}>
                          {performer.name}
                        </Typography>
                        <Typography sx={{ display: 'flex' }} color='textSecondary' component='div' noWrap>
                          {performer.taxonomies.map((taxonomy) => (
                            <Box mr={1} key={taxonomy.id}>
                              {t(`Taxonomies.${taxonomy.name}`)}
                            </Box>
                          ))}
                        </Typography>
                      </Stack>
                    </Paper>
                  </Grid>
                ))}
              </Grid>
            </Stack>
            <Grid item xs={12}>
              <iframe
                title='Event map'
                height='480'
                loading='lazy'
                style={{ width: '100%', border: 0, borderRadius: 24 }}
                src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyAmOzhjZJMKoedSf0dAsjyFTkuximT6aSo
    &q=${encodeURIComponent(`${event?.venue.name}, ${event?.venue.extended_address as string}`)}`}
              ></iframe>
            </Grid>
          </Grid>
          <Grid item xs={12} md={5} lg={4}>
            <Details event={event as Event} />
          </Grid>
        </Grid>
      </Container>
    </Stack>
  )
}
