'use client'
import useCreateEvent from '@/components/Event/useCreateEvent'
import { FormikField, PageHeading } from '@/components/Miscellaneous'
import {
  Container,
  Grid,
  InputAdornment,
  Typography,
  ListSubheader,
  MenuItem,
  OutlinedInput,
  InputLabel,
  FormControl,
  Select,
  Button,
  styled,
  Box
} from '@mui/material'
import { Formik } from 'formik'
import { useTranslations } from 'next-intl'
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker'
import { DateTime } from 'luxon'
import { DEFAULT_CURRENCY, PREDEFINED_CATEGORIES } from '@/app-config'
import Image from 'next/image'

export default function CreateEvent() {
  const t = useTranslations()
  const { initialValues, validationSchema, handleSubmit } = useCreateEvent()

  return (
    <Container sx={{ pt: 4 }}>
      <PageHeading>{t('Event.ShallWeStart')}</PageHeading>
      <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={handleSubmit}>
        {({ values, errors, touched, isValid, dirty, isSubmitting, setFieldValue, handleBlur }) => (
          <Grid container mt={0} spacing={3}>
            <Grid item xs={12} mb={-2}>
              <Typography variant='overline'>{t('Event.Main')}</Typography>
            </Grid>
            <Grid item xs={12} sm={4}>
              <FormikField label={t('Event.Title')} name='short_title' />
            </Grid>
            <Grid item xs={12} sm={4}>
              <DateTimePicker
                disablePast
                label={t('Event.Date')}
                value={DateTime.fromISO(values.datetime_utc)}
                onChange={(newValue) => setFieldValue('datetime_utc', newValue?.toISO())}
                slotProps={{
                  desktopPaper: { sx: { p: 0 } },
                  textField: {
                    fullWidth: true,
                    helperText: touched.datetime_utc && errors.datetime_utc ? String(errors.datetime_utc) : '',
                    onBlur: handleBlur,
                    error: touched.datetime_utc && Boolean(errors.datetime_utc)
                  }
                }}
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <FormikField
                label={t('Event.TicketPrice')}
                name='stats.lowest_price'
                type='number'
                InputProps={{ endAdornment: <InputAdornment position='end'>{DEFAULT_CURRENCY}</InputAdornment> }}
              />
            </Grid>

            <Grid item xs={12} mb={-2}>
              <Typography variant='overline'>{t('Event.AboutThePlace')}</Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormikField label={t('Event.City')} name='venue.city' />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormikField label={t('Event.State')} name='venue.state' />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormikField label={t('Event.Address')} name='venue.address' />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormikField label={t('Event.VenueName')} name='venue.name' />
            </Grid>

            <Grid item xs={12} mb={-2}>
              <Typography variant='overline'>{t('Event.AboutThePerformer')}</Typography>
            </Grid>
            <Grid item xs={12} sm={4}>
              <FormikField name='performers[0].name' label={t('Event.Performer')} />
            </Grid>
            <Grid item xs={12} sm={4}>
              <FormControl fullWidth>
                <InputLabel>{t('Event.Taxonomies')}</InputLabel>
                <Select
                  multiple
                  value={values.taxonomies}
                  onChange={(e) => {
                    const {
                      target: { value }
                    } = e
                    value.length <= 3 &&
                      setFieldValue('taxonomies', typeof value === 'string' ? value.split(',') : value)
                  }}
                  input={<OutlinedInput label={t('Event.Taxonomies')} />}
                >
                  {Object.keys(PREDEFINED_CATEGORIES).flatMap((categoryHeader) => [
                    <ListSubheader key={categoryHeader}>{t(`Taxonomies.${categoryHeader}`)}</ListSubheader>,
                    ...PREDEFINED_CATEGORIES[categoryHeader].map((mainCategory: string) => (
                      <MenuItem key={mainCategory} value={mainCategory}>
                        {t(`Taxonomies.${mainCategory}`)}
                      </MenuItem>
                    ))
                  ])}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Button variant='outlined' component='label' fullWidth sx={{ height: '100%' }}>
                {t('Event.PerformerImage')}
                <VisuallyHiddenInput
                  type='file'
                  onChange={async (e) => {
                    const file = (e.target.files as FileList)[0]
                    async function getBase64() {
                      return new Promise((resolve, reject) => {
                        const reader = new FileReader()
                        reader.readAsDataURL(file)
                        reader.onload = () => {
                          resolve(reader.result)
                        }
                        reader.onerror = reject
                      })
                    }
                    const base64 = await getBase64()
                    setFieldValue('performers[0].image', base64)
                  }}
                />
              </Button>
            </Grid>
            {values.performers?.[0].image && (
              <Grid item xs={12} justifyContent='end' display='flex'>
                <Box
                  width={128}
                  height={128}
                  position='relative'
                  sx={{
                    borderRadius: 2,
                    border: '1px solid',
                    overflow: 'hidden'
                  }}
                >
                  {values.performers?.[0].image && (
                    <Image
                      fill
                      style={{ objectFit: 'cover' }}
                      src={values.performers[0].image}
                      alt={t('Event.PerformerImage')}
                    />
                  )}
                </Box>
              </Grid>
            )}
            <Grid item xs={12}>
              <Button
                variant='contained'
                color='primary'
                fullWidth
                disabled={!isValid || !dirty || isSubmitting}
                onClick={() => handleSubmit(values)}
              >
                {t('Navigation.CreateEvent')}
              </Button>
            </Grid>
          </Grid>
        )}
      </Formik>
    </Container>
  )
}

const VisuallyHiddenInput = styled('input')({
  clip: 'rect(0 0 0 0)',
  clipPath: 'inset(50%)',
  height: 1,
  overflow: 'hidden',
  position: 'absolute',
  bottom: 0,
  left: 0,
  whiteSpace: 'nowrap',
  width: 1
})
