import { Details, Heading, Performer } from '@/components/Event'
import { Locale } from '@/types/locale'
import { DUMMY_TEXTS } from '@/utils/dummyTexts'
import fetchEvents from '@/utils/fetchEvents'
import { Container, Grid, Stack, Typography } from '@mui/material'
import { getTranslations, unstable_setRequestLocale } from 'next-intl/server'
import { notFound } from 'next/navigation'

export default async function Page({ params: { locale, eventId } }: { params: { eventId: string; locale: Locale } }) {
  unstable_setRequestLocale(locale)
  const t = await getTranslations()

  const eventsData = await fetchEvents({ id: eventId }, t('Event.NotFound'))
  const event = eventsData.events[0]

  if (!event) return notFound()

  return (
    <Stack>
      <Heading event={event} />
      <Container sx={{ mt: 8 }}>
        <Grid container spacing={{ xs: 5, md: 8 }} flexDirection={{ xs: 'column-reverse', md: 'row' }}>
          <Grid item xs={12} md={7} lg={8}>
            <Stack mb={4}>
              <Typography variant='h5' gutterBottom>
                {t('Event.Description')}
              </Typography>
              <Typography>{DUMMY_TEXTS.EVENT_DESCRIPTION}</Typography>
            </Stack>
            <Stack mb={4}>
              <Typography variant='h5' gutterBottom>
                {t('Event.AboutThePerformers')}
              </Typography>
              <Grid container spacing={2}>
                {event.performers.map((performer) => (
                  <Grid item xs={12} sm={6} key={performer.id}>
                    <Performer performer={performer} />
                  </Grid>
                ))}
              </Grid>
            </Stack>
            <Grid item xs={12}>
              <iframe
                title='Event map'
                height='480'
                loading='lazy'
                style={{ width: '100%', border: 0, borderRadius: 24 }}
                src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyAmOzhjZJMKoedSf0dAsjyFTkuximT6aSo
    &q=${encodeURIComponent(`${event.venue.name}, ${event.venue.extended_address as string}`)}`}
              ></iframe>
            </Grid>
          </Grid>
          <Grid item xs={12} md={5} lg={4}>
            <Details event={event} />
          </Grid>
        </Grid>
      </Container>
    </Stack>
  )
}
