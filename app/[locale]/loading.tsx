import { CircularProgress, Container } from '@mui/material'

export default function Loading() {
  return (
    <Container sx={{ textAlign: 'center', pt: 4 }}>
      <CircularProgress />
    </Container>
  )
}
