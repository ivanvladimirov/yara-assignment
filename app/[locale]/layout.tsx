import ThemeRegistry from '@/components/ThemeRegistry/ThemeRegistry'
import type { Metadata } from 'next'
import '@/translations/i18n'
import { Locale } from '@/types/locale'
import { DEFAULT_LOCALE, locales } from '@/app-config'
import { redirect } from '@/navigation'
import { Content, Footer, Navigation } from '@/components/Layout'
import { Box } from '@mui/material'
import { NextIntlClientProvider, useMessages } from 'next-intl'
import { CartProvider, WishlistProvider } from '@/contexts'
import { unstable_setRequestLocale } from 'next-intl/server'

export const metadata: Metadata = {
  title: 'Yara Assignment',
  description: 'React task implementing the SeatGeek API'
}

export function generateStaticParams() {
  return locales.map((locale) => ({ locale }))
}

export default function RootLayout({
  children,
  params: { locale }
}: {
  children: React.ReactNode
  params: { locale: Locale }
}) {
  unstable_setRequestLocale(locale)

  if (!locales.includes(locale)) redirect(`/${DEFAULT_LOCALE}`)

  const messages = useMessages()

  return (
    <html lang={locale}>
      <body>
        <NextIntlClientProvider locale={locale} messages={messages}>
          <ThemeRegistry locale={locale}>
            <CartProvider>
              <WishlistProvider>
                <Box display='flex' flexDirection='column' sx={{ minHeight: '100vh' }}>
                  <Navigation />
                  <Content>{children}</Content>
                  <Footer />
                </Box>
              </WishlistProvider>
            </CartProvider>
          </ThemeRegistry>
        </NextIntlClientProvider>
      </body>
    </html>
  )
}
