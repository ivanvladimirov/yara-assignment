'use client'
import { Typography, Container } from '@mui/material'

export default function Error({ error, reset }: { error: Error & { digest?: string }; reset: () => void }) {
  return (
    <Container>
      <Typography variant='h6' align='center'>
        {error.message ?? 'Oops, something went wrong :/'}
      </Typography>
    </Container>
  )
}
