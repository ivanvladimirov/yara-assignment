'use client'
import { PageHeading } from '@/components/Miscellaneous'
import { PDFDocument, WishlistItem, WishlistItemSkeleton } from '@/components/Wishlist'
import { useWishlist } from '@/contexts'
import { Event, FetchEventsResponse } from '@/types/event'
import objectToQueryString from '@/utils/objectToQueryString'
import { Button, Container, Stack } from '@mui/material'
import { useTranslations } from 'next-intl'
import { useEffect, useState, useCallback } from 'react'
import PictureAsPdfOutlinedIcon from '@mui/icons-material/PictureAsPdfOutlined'
import { PDFDownloadLink } from '@react-pdf/renderer'
import fetchEvents from '@/utils/fetchEvents'

export default function Wishlist() {
  const t = useTranslations('Wishlist')
  const { wishlist, updateWishlist } = useWishlist()
  const [isLoadingEvents, setIsLoadingEvents] = useState(true)

  const [events, setEvents] = useState<FetchEventsResponse['events']>([])
  const [isClient, setIsClient] = useState(false)

  useEffect(() => {
    setIsClient(true)
  }, [])

  const getWishlistEvents = useCallback(async () => {
    try {
      setIsLoadingEvents(true)
      const response = await fetchEvents({ id: wishlist.map((v) => v.toString()) })
      if (!!response.events.length) {
        setEvents(response.events)
      }
    } catch (error) {
      console.error(error)
    } finally {
      setIsLoadingEvents(false)
    }
  }, [wishlist])

  const removeItemFromWishlist = useCallback(
    (eventId: Event['id']) => {
      setEvents((state) => state.filter((event) => event.id !== eventId))
      updateWishlist(eventId)
    },
    [updateWishlist]
  )

  useEffect(() => {
    if (!!wishlist.length && !!!events.length) {
      getWishlistEvents()
    } else {
      setIsLoadingEvents(false)
    }
  }, [wishlist, getWishlistEvents, events])

  return (
    isClient && (
      <Container sx={{ pt: 4 }}>
        <Stack flexDirection='row' justifyContent='space-between' alignItems='center' mb={1}>
          <PageHeading>{t('Wishlist')}</PageHeading>
          <PDFDownloadLink document={<PDFDocument wishlist={events} />} fileName='my-wishlist.pdf'>
            {({ loading: isLoadingPDF }) => (
              <Button
                color='primary'
                disabled={isLoadingPDF || isLoadingEvents || !!!wishlist.length}
                startIcon={<PictureAsPdfOutlinedIcon />}
                variant='outlined'
              >
                {t('Download')}
              </Button>
            )}
          </PDFDownloadLink>
        </Stack>
        <Stack>
          {!isLoadingEvents
            ? events.map((event) => (
                <WishlistItem key={event.id} event={event} removeItemFromWishlist={removeItemFromWishlist} />
              ))
            : Array(2)
                .fill(undefined)
                .map((v, index) => <WishlistItemSkeleton key={index} />)}
        </Stack>
      </Container>
    )
  )
}
