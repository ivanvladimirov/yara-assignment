import { Events, TopContainer } from '@/components/Home'

export default async function Home() {
  return (
    <>
      <TopContainer />
      <Events />
    </>
  )
}
