import { EventsSkeleton } from '@/components/Miscellaneous'
import { Events, LoadMore, SearchFields } from '@/components/Search'
import { FetchEventsResponse } from '@/types/event'
import { Locale } from '@/types/locale'
import fetchEvents from '@/utils/fetchEvents'
import { Container, Typography, Box } from '@mui/material'
import { getTranslations, unstable_setRequestLocale } from 'next-intl/server'
import { Suspense } from 'react'

export default async function Search({
  params: { locale },
  searchParams
}: {
  params: { locale: Locale }
  searchParams: { [key: string]: string | string[] | undefined }
}) {
  unstable_setRequestLocale(locale)
  const t = await getTranslations('Search')

  const searchResult =
    !!searchParams.q || !!searchParams['taxonomies.name']
      ? await fetchEvents({ ...searchParams })
      : ({ events: [], meta: null } as unknown as FetchEventsResponse)

  return (
    <Container sx={{ pt: 4 }}>
      <SearchFields q={searchParams?.q as string} category={searchParams['taxonomies.name'] as string} />
      <Suspense fallback={<EventsSkeleton count={8} mt={4} />}>
        <Events events={searchResult} />
      </Suspense>
      {!!searchResult.meta && searchResult.meta.total > 0 && <LoadMore eventsCount={searchResult?.meta?.total ?? 0} />}
      {!!searchResult.meta && searchResult.meta.total === 0 && (
        <Typography variant='h6' align='center'>
          {t('NotResultsFound')}
        </Typography>
      )}
      {!!!searchParams.q && !!!searchParams['taxonomies.name'] && (
        <Box display='flex' flexDirection='column' justifyContent='center'>
          <Typography variant='h6' align='center'>
            {t('LetsFindYourNextEvent')}
          </Typography>
          <Typography variant='h6' align='center'>
            {t('StartWithCategoryOrQuery')}
          </Typography>
        </Box>
      )}
    </Container>
  )
}
