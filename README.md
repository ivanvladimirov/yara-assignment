## Getting Started

First create an .env file (cp .env.example) and set your SeatGeek API public key

Then, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

The project is also deployed at [yara-assignment.vercel.app](https://yara-assignment.vercel.app/bg) with an already set API key
