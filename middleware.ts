import createMiddleware from 'next-intl/middleware'
import { DEFAULT_LOCALE, locales } from '@/app-config'

export default createMiddleware({
  locales,
  defaultLocale: DEFAULT_LOCALE
})

export const config = {
  matcher: ['/', '/(bg|en)/:path*']
}
