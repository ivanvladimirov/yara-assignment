import { useCallback, useMemo } from 'react'
import { useSearchParams } from 'next/navigation'
import { useRouter, usePathname } from '@/navigation'
import objectToQueryString from '@/utils/objectToQueryString'
import { EventQueryParams } from '@/types/event'

export const PER_PAGE_DEFAULT = '8'
export const MAX_SEARCH_RESULTS = 50

export default function useSearch() {
  const searchParams = useSearchParams()
  const router = useRouter()
  const pathname = usePathname()

  const getExistingQueryObject = useCallback(() => {
    const paramsObject: { [key: string]: string } = {}

    for (const [key, value] of searchParams) {
      paramsObject[key] = value
    }

    return paramsObject
  }, [searchParams])

  const existingSearchQuery = getExistingQueryObject()
  const defaultSearchQuery: Partial<EventQueryParams> = useMemo(
    () => ({
      per_page: PER_PAGE_DEFAULT
    }),
    []
  )

  const handleSubmit = useCallback(
    (searchObject: Partial<EventQueryParams>) => {
      const mergedSearchObjects = {
        ...defaultSearchQuery,
        ...existingSearchQuery,
        ...searchObject
      }

      const searchQuery = objectToQueryString(mergedSearchObjects)

      // @ts-ignore
      // We ignore it because this router comes from next-intl types
      // and is not updated with what next/navigation's router is providing
      router.push(`${pathname}?${searchQuery}`, { scroll: false })
    },
    [defaultSearchQuery, existingSearchQuery, pathname, router]
  )

  return {
    searchParams,
    handleSubmit
  }
}
