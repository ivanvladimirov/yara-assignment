'use client'

import { Event as EventComponent } from '@/components/Miscellaneous'
import type { FetchEventsResponse } from '@/types/event'
import { Grid } from '@mui/material'

export default function Events({ events }: { events: FetchEventsResponse }) {
  return (
    <Grid container spacing={3} mt={4}>
      {events.events.map((event) => (
        <Grid key={event.id} item xs={6} sm={4} md={3}>
          <EventComponent event={event} />
        </Grid>
      ))}
    </Grid>
  )
}
