'use client'

import { Button } from '@mui/material'
import { useTranslations } from 'next-intl'
import useSearch, { MAX_SEARCH_RESULTS, PER_PAGE_DEFAULT } from '../useSearch'

export default function LoadMore({ eventsCount }: { eventsCount: number }) {
  const t = useTranslations('Search')
  const { searchParams, handleSubmit } = useSearch()
  const per_page = (searchParams.get('per_page') as string) ?? PER_PAGE_DEFAULT

  return (
    <Button
      sx={{ mt: 2 }}
      variant='outlined'
      disabled={MAX_SEARCH_RESULTS <= +per_page || eventsCount < +per_page}
      onClick={() => handleSubmit({ per_page: (+per_page + +PER_PAGE_DEFAULT).toString() })}
    >
      {t('LoadMore')}
    </Button>
  )
}
