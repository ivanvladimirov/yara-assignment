'use client'

import { InputBase, styled } from '@mui/material'

const SearchTextField = styled(InputBase)(({ theme }) => ({
  '&.MuiInputBase-root': {
    backgroundColor: theme.palette.mode === 'light' ? theme.palette.grey[200] : '#1f262f',
    borderRadius: 8,
    paddingLeft: '12px',
    paddingRight: '12px',
    transition: theme.transitions.create(['border-color', 'background-color', 'box-shadow']),
    '&:hover': {
      backgroundColor: theme.palette.mode === 'light' ? theme.palette.grey[300] : '#1f262f'
    },
    '&:focus': {
      backgroundColor: theme.palette.mode === 'light' ? theme.palette.grey[300] : '#1f262f'
    },
    '& svg': {
      color: theme.palette.grey[500]
    }
  },
  '& .MuiInputBase-input': {
    position: 'relative',
    border: 'none',
    fontSize: 16,
    maxHeight: 22,
    width: '100%',
    color: theme.palette.mode === 'light' ? theme.palette.grey[500] : '#101318',
    padding: '13px 12px',
    paddingLeft: 0,
    transition: theme.transitions.create(['border-color', 'background-color', 'box-shadow'])
  }
}))

export default SearchTextField
