'use client'

import { useState } from 'react'
import { Box, Button, FormControl, InputAdornment, Select, MenuItem, IconButton, ListSubheader } from '@mui/material'
import useSearch from '../useSearch'
import { SearchTextField } from './components'
import SearchIcon from '@mui/icons-material/Search'
import { useTranslations } from 'next-intl'
import CategoryOutlinedIcon from '@mui/icons-material/CategoryOutlined'
import { PREDEFINED_CATEGORIES } from '@/app-config'
import CloseOutlinedIcon from '@mui/icons-material/CloseOutlined'

const ALL_CATEGORY_VALUE = ''

export default function SearchFields({
  q = '',
  category: initialCategory = ALL_CATEGORY_VALUE
}: {
  q?: string
  category?: string
}) {
  const t = useTranslations()
  const { handleSubmit } = useSearch()

  const [query, setQuery] = useState<string>(q)
  const [category, setCategory] = useState<string>(initialCategory)

  return (
    <Box
      display='flex'
      gap={2.5}
      component='form'
      onSubmit={(e) => {
        e.preventDefault()
        handleSubmit({
          q: query,
          'taxonomies.name': category === ALL_CATEGORY_VALUE ? '' : category,
          per_page: '8'
        })
      }}
      flexDirection={{ xs: 'column', sm: 'row' }}
    >
      <SearchTextField
        startAdornment={
          <InputAdornment position='start'>
            <SearchIcon />
          </InputAdornment>
        }
        endAdornment={
          <InputAdornment position='end'>
            <IconButton
              disabled={!!!query}
              onClick={() => {
                setQuery('')
                handleSubmit({
                  q: '',
                  per_page: '8'
                })
              }}
            >
              <CloseOutlinedIcon />
            </IconButton>
          </InputAdornment>
        }
        value={query}
        onChange={(e) => setQuery(e.target.value)}
        fullWidth
        placeholder={t('Search.WhatDoYouWantToWatchLive')}
      />
      <FormControl fullWidth>
        <Select
          sx={{
            '& .MuiSelect-select': {
              width: '100%'
            }
          }}
          displayEmpty
          startAdornment={
            <InputAdornment position='start'>
              <CategoryOutlinedIcon />
            </InputAdornment>
          }
          value={category}
          onChange={(e) => setCategory(e.target.value)}
          input={<SearchTextField />}
        >
          <MenuItem value={ALL_CATEGORY_VALUE}>{t('Search.AllCategories')}</MenuItem>
          {Object.keys(PREDEFINED_CATEGORIES).flatMap((categoryHeader) => [
            <ListSubheader key={categoryHeader}>{t(`Taxonomies.${categoryHeader}`)}</ListSubheader>,
            ...PREDEFINED_CATEGORIES[categoryHeader].map((mainCategory: string) => (
              <MenuItem key={mainCategory} value={mainCategory}>
                {t(`Taxonomies.${mainCategory}`)}
              </MenuItem>
            ))
          ])}
        </Select>
      </FormControl>
      <Button
        color='primary'
        variant='contained'
        sx={{
          minWidth: 48,
          width: { xs: '100%', sm: 48 },
          height: 48,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          p: 0
        }}
        type='submit'
      >
        <SearchIcon fontSize='large' />
      </Button>
    </Box>
  )
}
