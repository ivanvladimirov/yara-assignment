'use client'
import { Box, Container as MuiContainer, Grid, Stack } from '@mui/material'
import { EventsSummary, Heading, SearchField } from './components'
import Image from 'next/image'

const ADDITIONAL_HEIGHT_FOR_PADDING = 40

export default function TopContainer() {
  return (
    <Box
      sx={(theme) => ({
        mt: `${-theme.sizing.header.height}px`,
        background:
          'linear-gradient(to bottom, rgba(22, 28, 36, 0.8), rgba(22, 28, 36, 0.8)),url(/images/homepage_gradient.jpg)',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        height: '100vh',
        minHeight: 745,
        pt: `${theme.sizing.header.height + ADDITIONAL_HEIGHT_FOR_PADDING}px`
      })}
    >
      <MuiContainer
        maxWidth='lg'
        sx={(theme) => ({
          display: 'flex',
          alignItems: 'center',
          height: `calc(100vh - ${
            theme.sizing.header.height + theme.sizing.header.height + ADDITIONAL_HEIGHT_FOR_PADDING
          }px)`,
          minHeight: 500
        })}
      >
        <Grid container justifyContent='space-between'>
          <Grid item xs={12} md={5}>
            <Stack>
              <Heading />
              <SearchField />
              <EventsSummary />
            </Stack>
          </Grid>
          <Grid item xs={12} md={5} sx={{ position: 'relative', display: { xs: 'none', md: 'initial' } }}>
            <Image alt='homepage-events' src='/images/homepage_events.svg' fill style={{ objectFit: 'contain' }} />
          </Grid>
        </Grid>
      </MuiContainer>
    </Box>
  )
}
