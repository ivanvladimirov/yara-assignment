import { useTranslations } from 'next-intl'
import { Box, Typography } from '@mui/material'

export default function Heading() {
  const t = useTranslations('Home')

  return (
    <>
      <Typography variant='h2' color='common.white'>
        {t('FindThe')}{' '}
        <Box component='span' sx={{ color: 'primary.main' }}>
          {t('Event')}
        </Box>
        <br />
        {t('YoullLove')}
      </Typography>
      <Typography color='grey.500' mt={3} mb={4.5}>
        {t('BriefInformation')}
      </Typography>
    </>
  )
}
