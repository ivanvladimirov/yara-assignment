import { Box, Divider, Stack, Typography } from '@mui/material'
import { ReactNode } from 'react'
import { useTranslations } from 'next-intl'

export default function EventsSummary() {
  const t = useTranslations('Home')

  return (
    <Stack mt={8}>
      <Typography variant='h5' mb={2} color='common.white'>
        {t('2023AtAGlance')}
      </Typography>
      <Box display='flex' alignItems='center' justifyContent='space-between'>
        <Item top='5m+' bottom={t('Events')} />
        <Divider orientation='vertical' flexItem sx={{ borderColor: 'common.white', opacity: 0.15 }} />
        <Item top='798k+' bottom={t('PaidCreators')} />
        <Divider orientation='vertical' flexItem sx={{ borderColor: 'common.white', opacity: 0.15 }} />
        <Item top='284m+' bottom={t('TotalTicketsSold')} />
        <Divider orientation='vertical' flexItem sx={{ borderColor: 'common.white', opacity: 0.15 }} />
        <Item top='180+' bottom={t('Countries')} />
      </Box>
    </Stack>
  )
}

const Item = ({ top, bottom }: { top: ReactNode; bottom: ReactNode }) => {
  return (
    <Stack>
      <Typography variant='h4' component='div' color='common.white'>
        {top}
      </Typography>
      <Typography variant='body2' sx={{ opacity: 0.5 }} component='div' color='common.white'>
        {bottom}
      </Typography>
    </Stack>
  )
}
