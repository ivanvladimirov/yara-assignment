'use client'
import { Box, Button, InputAdornment, TextField } from '@mui/material'
import SearchIcon from '@mui/icons-material/Search'
import { useTranslations } from 'next-intl'
import { FormEvent, useState } from 'react'
import { useRouter } from '@/navigation'

export default function SearchField() {
  const t = useTranslations('Home')
  const router = useRouter()

  const [value, setValue] = useState('')

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    router.push(`/search?q=${value}&per_page=8`)
  }

  return (
    <Box
      display='flex'
      alignItems='center'
      sx={{ height: 70, borderRadius: 2, p: 1.5, bgcolor: 'common.white' }}
      component='form'
      onSubmit={handleSubmit}
    >
      <TextField
        value={value}
        onChange={(e) => setValue(e.target.value)}
        variant='standard'
        autoFocus
        InputProps={{
          disableUnderline: true,
          startAdornment: (
            <InputAdornment position='start'>
              <SearchIcon fontSize='large' />
            </InputAdornment>
          )
        }}
        placeholder={t('SearchEvents')}
        fullWidth
        sx={{
          height: '100%',
          '& .MuiInputBase-root': {
            height: '100%'
          },
          '& .MuiInputBase-input': {
            p: 0
          }
        }}
      />
      <Button
        color='primary'
        variant='contained'
        sx={{
          minWidth: 48,
          width: 48,
          height: 48,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          p: 0
        }}
        type='submit'
      >
        <SearchIcon fontSize='large' />
      </Button>
    </Box>
  )
}
