import { Event } from '@/components/Miscellaneous'
import fetchEvents from '@/utils/fetchEvents'
import { Grid } from '@mui/material'

export default async function TrendingEvents() {
  const trendingEventsData = await fetchEvents({ per_page: '8', 'score.gt': '0.75' })

  return (
    <Grid container spacing={3} mt={4}>
      {trendingEventsData.events.map((event) => (
        <Grid key={event.id} item xs={6} sm={4} md={3}>
          <Event event={event} />
        </Grid>
      ))}
    </Grid>
  )
}
