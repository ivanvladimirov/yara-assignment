'use client'
import { Box, Button } from '@mui/material'
import { useRouter } from '@/navigation'
import { useTranslations } from 'next-intl'
import { PageHeading } from '@/components/Miscellaneous'

export default function TaxonomyHeading({ title, href }: { title: string; href?: string }) {
  const router = useRouter()
  const t = useTranslations()

  return (
    <Box display='flex' justifyContent='space-between' alignItems='center' sx={{ width: '100%' }} mt={8}>
      <PageHeading>{title}</PageHeading>
      {!!href && (
        <Button onClick={() => router.push(href)} variant='outlined'>
          {t('GoTo')} {title.toLowerCase()}
        </Button>
      )}
    </Box>
  )
}
