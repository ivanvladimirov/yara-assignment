import { Event } from '@/components/Miscellaneous'
import fetchEvents from '@/utils/fetchEvents'
import { Grid } from '@mui/material'

export default async function Comedy() {
  const comedyData = await fetchEvents({
    per_page: '3',
    'taxonomies.name': 'comedy'
  })

  return (
    <Grid container spacing={3} mt={0}>
      {comedyData.events.map((event) => (
        <Grid key={event.id} item xs={6} sm={4}>
          <Event event={event} />
        </Grid>
      ))}
    </Grid>
  )
}
