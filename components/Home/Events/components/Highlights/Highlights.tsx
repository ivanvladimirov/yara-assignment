import { Event } from '@/components/Miscellaneous'
import fetchEvents from '@/utils/fetchEvents'
import { Grid } from '@mui/material'

export default async function Highlights() {
  const highlightsData = await fetchEvents({
    per_page: '2',
    'score.gt': '0.85'
  })

  return (
    <Grid container spacing={3} mt={0}>
      {highlightsData.events.map((event) => (
        <Grid key={event.id} item xs={6}>
          <Event event={event} />
        </Grid>
      ))}
    </Grid>
  )
}
