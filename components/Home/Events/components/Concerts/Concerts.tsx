import { Event } from '@/components/Miscellaneous'
import fetchEvents from '@/utils/fetchEvents'
import { Grid } from '@mui/material'

export default async function Concerts() {
  const concertsData = await fetchEvents({
    per_page: '3',
    'taxonomies.name': 'concert'
  })

  return (
    <Grid container spacing={3} mt={0}>
      {concertsData.events.map((event) => (
        <Grid key={event.id} item xs={6} sm={4}>
          <Event event={event} />
        </Grid>
      ))}
    </Grid>
  )
}
