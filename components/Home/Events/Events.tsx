import { Typography, Container } from '@mui/material'
import { getTranslations } from 'next-intl/server'
import { TrendingEvents, Concerts, TaxonomyHeading, Sports, Comedy, Highlights } from './components'
import { Suspense } from 'react'
import { EventsSkeleton } from '@/components/Miscellaneous'

export default async function Events() {
  const t = await getTranslations()

  return (
    <Container sx={{ mt: 16, display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <Typography variant='h2' align='center' gutterBottom>
        {t('Home.TrendingEvents')}
      </Typography>
      <Typography variant='body1' color='textSecondary' maxWidth={480} align='center'>
        {t('Home.TrendingEventsDescription')}
      </Typography>
      <Suspense fallback={<EventsSkeleton count={8} mt={4} />}>
        <TrendingEvents />
      </Suspense>

      <TaxonomyHeading title={t('Taxonomies.Highlights')} />
      <Suspense fallback={<EventsSkeleton count={2} mt={0} />}>
        <Highlights />
      </Suspense>

      <TaxonomyHeading title={t('Taxonomies.Concerts')} href={'/search?taxonomies.name=concert&per_page=8'} />
      <Suspense fallback={<EventsSkeleton count={3} mt={0} />}>
        <Concerts />
      </Suspense>

      <TaxonomyHeading title={t('Taxonomies.Sports')} href={'/search?taxonomies.name=sports&per_page=8'} />
      <Suspense fallback={<EventsSkeleton count={3} mt={0} />}>
        <Sports />
      </Suspense>

      <TaxonomyHeading title={t('Taxonomies.Comedy')} href={'/search?taxonomies.name=comedy&per_page=8'} />
      <Suspense fallback={<EventsSkeleton count={3} mt={0} />}>
        <Comedy />
      </Suspense>
    </Container>
  )
}
