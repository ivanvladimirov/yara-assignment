import { Event } from '@/types/event'
import { Page, Text, Document, StyleSheet, Font, View } from '@react-pdf/renderer'
import { DateTime } from 'luxon'

Font.register({
  family: 'Oswald',
  src: 'https://fonts.gstatic.com/s/oswald/v13/Y_TKV6o8WovbUd3m_X9aAA.ttf'
})

const styles = StyleSheet.create({
  page: {
    flexDirection: 'column',
    paddingTop: 35,
    paddingBottom: 65,
    paddingHorizontal: 35,
    fontFamily: 'Oswald'
  },
  header: {
    fontSize: 20,
    marginBottom: 20,
    textAlign: 'center',
    color: 'grey'
  },
  title: {
    fontSize: 16
  },
  text: {
    fontSize: 13
  },
  container: {
    flexDirection: 'row'
  },
  wishlistItem: {
    marginBottom: '14px'
  },
  pageNumber: {
    position: 'absolute',
    fontSize: 12,
    bottom: 30,
    left: 0,
    right: 0,
    textAlign: 'center',
    color: 'grey'
  }
})

export default function PDFDocument({ wishlist }: { wishlist: Event[] }) {
  return (
    <Document>
      <Page size='A4' style={styles.page}>
        <Text style={styles.header} fixed>
          My wishlist
        </Text>
        {wishlist.map((event) => (
          <View key={event.id} style={styles.wishlistItem}>
            <Text style={styles.title}>{event.title}</Text>
            <View style={styles.container}>
              <Text style={styles.text}>
                Date: {DateTime.fromISO(event.datetime_utc).toLocaleString({ dateStyle: 'long', timeStyle: 'short' })}
              </Text>
              <Text style={{ marginLeft: '16px' }}></Text>
              <Text style={styles.text}>
                Location: {event.venue.name} | {event.venue.state}
              </Text>
            </View>
          </View>
        ))}
        <Text
          style={styles.pageNumber}
          render={({ pageNumber, totalPages }) => `Yara Assignment - Page ${pageNumber} / ${totalPages}`}
          fixed
        />
      </Page>
    </Document>
  )
}
