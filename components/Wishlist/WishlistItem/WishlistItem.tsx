'use client'
import { DisplayDate, WishlistIconButton } from '@/components/Miscellaneous'
import { Event } from '@/types/event'
import { Box, Stack, Tooltip, Typography } from '@mui/material'
import { useTranslations } from 'next-intl'
import { useRouter } from '@/navigation'

export default function WishlistItem({
  event,
  removeItemFromWishlist
}: {
  event: Event
  removeItemFromWishlist: (eventId: Event['id']) => void
}) {
  const router = useRouter()
  const t = useTranslations('Wishlist')

  return (
    <Stack
      flexDirection='row'
      alignItems='center'
      mx={-2.5}
      px={2.5}
      py={1.25}
      sx={(theme) => ({
        borderRadius: 2,
        cursor: 'pointer',
        '&:hover': {
          boxShadow: theme.shadows[1]
        }
      })}
    >
      <Stack width={120} onClick={() => router.push(`/event/${event.id}`)}>
        <Typography variant='h6'>
          <DisplayDate date={event.datetime_utc} options={{ month: 'short', day: 'numeric' }} />
        </Typography>
        <Typography color='textSecondary'>
          <DisplayDate date={event.datetime_utc} options={{ weekday: 'short' }} /> ·{' '}
          <DisplayDate date={event.datetime_utc} options={{ timeStyle: 'short' }} />
        </Typography>
      </Stack>
      <Stack width={'100%'} onClick={() => router.push(`/event/${event.id}`)}>
        <Typography variant='h6'>{event.title}</Typography>
        <Typography color='textSecondary'>
          {event.venue.name} · {event.venue.state}
        </Typography>
      </Stack>
      <Box>
        <Tooltip title={t('RemoveFromWishlist')}>
          <WishlistIconButton eventId={event.id} onClick={() => removeItemFromWishlist(event.id)} />
        </Tooltip>
      </Box>
    </Stack>
  )
}
