import { Skeleton, Stack, Typography } from '@mui/material'

export default function WishlistItemSkeleton() {
  return (
    <Stack
      flexDirection='row'
      mx={-2.5}
      px={2.5}
      py={1.25}
      sx={(theme) => ({
        borderRadius: 2
      })}
    >
      <Stack width={120}>
        <Typography variant='h6'>
          <Skeleton width='90%' />
        </Typography>
        <Typography color='textSecondary'>
          <Skeleton width='75%' />
        </Typography>
      </Stack>
      <Stack width={'100%'}>
        <Typography variant='h6'>
          <Skeleton width='20%' />
        </Typography>
        <Typography color='textSecondary'>
          <Skeleton width='40%' />
        </Typography>
      </Stack>
    </Stack>
  )
}
