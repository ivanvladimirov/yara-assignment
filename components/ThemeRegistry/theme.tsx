import { Source_Sans_3 } from 'next/font/google'
import { Components, PaletteOptions, createTheme } from '@mui/material/styles'
import sizing from './sizing'
import palette from './palette'
import components from './components'

const SourceSans3 = Source_Sans_3({
  weight: ['300', '400', '500', '600', '700'],
  subsets: ['latin', 'cyrillic'],
  display: 'swap'
})

declare module '@mui/material/styles' {
  interface Theme {
    sizing: {
      header: {
        height: number
        collapsedHeight: number
      }
    }
    customShadows: {
      [key: number]: string
    }
  }
  interface ThemeOptions {
    sizing: {
      header: {
        height: number
        collapsedHeight: number
      }
    }
    customShadows: {
      [key: number]: string
    }
  }
}

const theme = createTheme({
  palette: palette as PaletteOptions,
  components: components as Components,
  sizing,
  customShadows: { 1: 'rgba(145, 158, 171, 0.2) 0px 0px 2px 0px, rgba(145, 158, 171, 0.12) 0px 12px 24px -4px' },
  typography: {
    fontFamily: SourceSans3.style.fontFamily,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
    fontWeightBold: 700,
    h1: {
      fontWeight: 700,
      lineHeight: 1.25,
      fontSize: '2.5rem',
      '@media (min-width:600px)': {
        fontSize: '3.25rem'
      },
      '@media (min-width:900px)': {
        fontSize: '3.625rem'
      },
      '@media (min-width:1200px)': {
        fontSize: '4rem'
      }
    },
    h2: {
      fontWeight: 700,
      lineHeight: 1.3,
      fontSize: '2rem',
      '@media (min-width:600px)': {
        fontSize: '2.5rem'
      },
      '@media (min-width:900px)': {
        fontSize: '2.75rem'
      },
      '@media (min-width:1200px)': {
        fontSize: '3rem'
      }
    },
    h3: {
      fontWeight: 700,
      lineHeight: 1.5,
      fontSize: '1.5rem',
      '@media (min-width:600px)': {
        fontSize: '1.625rem'
      },
      '@media (min-width:900px)': {
        fontSize: '1.875rem'
      },
      '@media (min-width:1200px)': {
        fontSize: '2rem'
      }
    },
    h4: {
      fontWeight: 600,
      lineHeight: 1.5,
      fontSize: '1.25rem',
      '@media (min-width:600px)': {
        fontSize: '1.25rem'
      },
      '@media (min-width:900px)': {
        fontSize: '1.5rem'
      },
      '@media (min-width:1200px)': {
        fontSize: '1.5rem'
      }
    },
    h5: {
      fontWeight: 600,
      lineHeight: 1.5,
      fontSize: '1.125rem',
      '@media (min-width:600px)': {
        fontSize: '1.1875rem'
      },
      '@media (min-width:900px)': {
        fontSize: '1.25rem'
      },
      '@media (min-width:1200px)': {
        fontSize: '1.25rem'
      }
    },
    h6: {
      fontWeight: 600,
      lineHeight: 1.56,
      fontSize: '1.0625rem',
      '@media (min-width:600px)': {
        fontSize: '1.125rem'
      },
      '@media (min-width:900px)': {
        fontSize: '1.125rem'
      },
      '@media (min-width:1200px)': {
        fontSize: '1.125rem'
      }
    },
    subtitle1: {
      fontWeight: 600,
      lineHeight: 1.5,
      fontSize: '1rem'
    },
    subtitle2: {
      fontWeight: 600,
      lineHeight: 1.57,
      fontSize: '0.875rem'
    },
    body1: {
      lineHeight: 1.5,
      fontSize: '1rem',
      fontWeight: 400
    },
    body2: {
      lineHeight: 1.57,
      fontSize: '0.875rem',
      fontWeight: 400
    },
    caption: {
      lineHeight: 1.5,
      fontSize: '0.75rem',
      fontWeight: 400
    },
    overline: {
      fontWeight: 700,
      lineHeight: 1.5,
      fontSize: '0.75rem',
      textTransform: 'uppercase'
    },
    button: {
      fontWeight: 600,
      lineHeight: 1.71,
      fontSize: '0.938rem',
      textTransform: 'capitalize'
    }
  }
})

export default theme
