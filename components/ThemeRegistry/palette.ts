const palette = {
  mode: 'light',
  primary: {
    main: '#FA541C'
  },
  text: {
    primary: '#212B36',
    secondary: '#637381',
    disabled: '#919EAB'
  },
  grey: {
    '0': '#FFFFFF',
    '50': '#fafafa',
    '100': '#F9FAFB',
    '200': '#F4F6F8',
    '300': '#DFE3E8',
    '400': '#C4CDD5',
    '500': '#919EAB',
    '600': '#637381',
    '700': '#454F5B',
    '800': '#212B36',
    '900': '#161C24',
    A100: '#f5f5f5',
    A200: '#eeeeee',
    A400: '#bdbdbd',
    A700: '#616161'
  }
}

export default palette
