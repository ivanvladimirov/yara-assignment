const components = {
  MuiButton: {
    defaultProps: {
      color: 'inherit',
      disableElevation: true
    },
    styleOverrides: {
      root: {
        textTransform: 'none',
        borderRadius: 8
      }
    }
  },
  MuiDivider: {
    styleOverrides: {
      root: {
        borderStyle: 'dashed'
      }
    }
  },
  MuiLink: {
    defaultProps: {
      color: 'inherit'
    },
    styleOverrides: {
      root: {
        textDecoration: 'none',
        '&:hover': {
          textDecoration: 'underline'
        }
      }
    }
  },
  MuiPaper: {
    defaultProps: {
      elevation: 0
    },
    styleOverrides: {
      root: {
        borderRadius: 16
      }
    }
  }
}

export default components
