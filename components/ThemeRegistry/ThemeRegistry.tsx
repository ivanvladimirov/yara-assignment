'use client'
import * as React from 'react'
import { ThemeProvider } from '@mui/material/styles'
import CssBaseline from '@mui/material/CssBaseline'
import NextAppDirEmotionCacheProvider from './EmotionCache'
import theme from './theme'
import { LocalizationProvider } from '@mui/x-date-pickers'
import { AdapterLuxon } from '@mui/x-date-pickers/AdapterLuxon'
import { Locale } from '@/types/locale'
import { SnackbarProvider } from 'notistack'

export default function ThemeRegistry({ children, locale }: { children: React.ReactNode; locale: Locale }) {
  return (
    <LocalizationProvider dateAdapter={AdapterLuxon} adapterLocale={locale}>
      <NextAppDirEmotionCacheProvider options={{ key: 'mui' }}>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <SnackbarProvider preventDuplicate />
          {children}
        </ThemeProvider>
      </NextAppDirEmotionCacheProvider>
    </LocalizationProvider>
  )
}
