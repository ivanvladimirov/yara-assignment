import { Performer } from '@/types/event'
import { Box, Paper, Stack, Typography } from '@mui/material'
import { getMessages, getTranslations } from 'next-intl/server'
import Image from 'next/image'

export default async function Performer({ performer }: { performer: Performer }) {
  const t = await getTranslations()
  const messages = await getMessages()

  return (
    <Paper sx={{ display: 'flex', alignItems: 'center', border: '1px solid rgba(145, 158, 171, 0.16)', p: 2 }}>
      <Box sx={{ position: 'relative', minWidth: 64, minHeight: 64, borderRadius: 2.5, overflow: 'hidden' }}>
        {performer?.image && (
          <Image fill alt={performer?.name} src={performer?.image as string} style={{ objectFit: 'cover' }} />
        )}
      </Box>
      <Stack ml={1.5} overflow='hidden'>
        <Typography variant='h6' noWrap title={performer.name}>
          {performer.name}
        </Typography>
        <Typography sx={{ display: 'flex' }} color='textSecondary' component='div' noWrap>
          {performer.taxonomies.map((taxonomy) => (
            <Box mr={1} key={taxonomy.id}>
              {taxonomy.name in messages['Taxonomies']
                ? t(`Taxonomies.${taxonomy.name}`)
                : taxonomy.name
                    .split('_')
                    .map((string) => string.replace(/\b\w/g, (char) => char.toUpperCase()))
                    .join(' ')}
            </Box>
          ))}
        </Typography>
      </Stack>
    </Paper>
  )
}
