export { default as Details } from './Details'
export { default as Heading } from './Heading'
export { default as Performer } from './Performer'
