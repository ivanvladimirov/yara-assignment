import { Event } from '@/types/event'
import { Box, Container, Stack, Typography } from '@mui/material'
import { getMessages, getTranslations } from 'next-intl/server'
import { GetTickets } from './components'

export default async function Heading({ event }: { event: Event }) {
  const t = await getTranslations()
  const messages = await getMessages()

  return (
    <Box
      sx={{
        background:
          'linear-gradient(to bottom, rgba(22, 28, 36, 0.8), rgba(22, 28, 36, 0.8)),url(/images/homepage_gradient.jpg)',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        color: 'common.white'
      }}
    >
      <Container
        sx={{
          minHeight: 300,
          display: 'flex',
          flexDirection: { xs: 'column', md: 'row' },
          alignItems: 'center',
          justifyContent: { xs: 'center', md: 'space-between' }
        }}
      >
        <Stack mb={{ xs: 2, md: 0 }}>
          <Typography variant='h3' sx={{ textAlign: { xs: 'center', md: 'left' } }}>
            {event.short_title}
          </Typography>
          <Typography
            sx={{ opacity: 0.5, display: 'flex', justifyContent: { xs: 'center', md: 'left' } }}
            component='div'
          >
            {event.taxonomies.map((taxonomy) => (
              <Box mr={3} key={taxonomy.id} sx={{ '&:last-of-type': { mr: 0 } }}>
                {taxonomy.name in messages['Taxonomies']
                  ? t(`Taxonomies.${taxonomy.name}`)
                  : taxonomy.name
                      .split('_')
                      .map((string) => string.replace(/\b\w/g, (char) => char.toUpperCase()))
                      .join(' ')}
              </Box>
            ))}
          </Typography>
        </Stack>
        <GetTickets event={event} />
      </Container>
    </Box>
  )
}
