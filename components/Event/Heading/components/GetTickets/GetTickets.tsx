'use client'
import { WishlistIconButton } from '@/components/Miscellaneous'
import { Event } from '@/types/event'
import { Box, Button, IconButton, Stack, Typography } from '@mui/material'
import RemoveIcon from '@mui/icons-material/Remove'
import AddIcon from '@mui/icons-material/Add'
import { useTranslations } from 'next-intl'
import { useCallback, useState } from 'react'
import { MAX_TICKETS_TO_BUY } from '@/app-config'
import { useCart } from '@/contexts'
import { enqueueSnackbar } from 'notistack'

export default function GetTickets({ event }: { event: Event }) {
  const { addToCart, setOpenCart } = useCart()

  const t = useTranslations('Event')
  const noListedPrice = event.stats.lowest_price === null || event.stats.lowest_price === 0

  const [quantity, setQuantity] = useState(1)

  const addQuantity = useCallback(() => {
    setQuantity((state) => (state >= MAX_TICKETS_TO_BUY ? MAX_TICKETS_TO_BUY : state + 1))
  }, [])

  const removeQuantity = useCallback(() => {
    setQuantity((state) => (state <= 1 ? 1 : state - 1))
  }, [])

  return (
    <Stack maxWidth={340} width='100%'>
      <Button
        variant='contained'
        color='primary'
        fullWidth
        size='large'
        sx={{ mr: 1 }}
        onClick={() => {
          if (!noListedPrice) {
            addToCart({
              name: event.short_title,
              quantity,
              id: event.id,
              price: event.stats.lowest_price
            })
            setOpenCart(true)
          }
        }}
      >
        {noListedPrice ? t('NoTicketsYet') : t('GetTickets')}
      </Button>
      <Box display='flex' alignItems='center' justifyContent='space-between'>
        <Box display='flex' alignItems='center'>
          <IconButton size='small' disabled={noListedPrice || quantity <= 1} onClick={removeQuantity}>
            <RemoveIcon sx={{ color: 'common.white' }} />
          </IconButton>
          <Typography mx={1} sx={{ cursor: 'default', fontSize: '1.25rem' }}>
            {quantity}
          </Typography>
          <IconButton size='small' disabled={noListedPrice || quantity >= MAX_TICKETS_TO_BUY} onClick={addQuantity}>
            <AddIcon sx={{ color: 'common.white' }} />
          </IconButton>
        </Box>
        <WishlistIconButton eventId={event.id} />
      </Box>
    </Stack>
  )
}
