'use client'
import { Event } from '@/types/event'
import { Divider, Paper } from '@mui/material'
import { useTranslations } from 'next-intl'
import { DisplayCurrency, DisplayDate } from '@/components/Miscellaneous'
import { ListItem } from './components'
import CalendarTodayOutlinedIcon from '@mui/icons-material/CalendarTodayOutlined'
import FmdGoodOutlinedIcon from '@mui/icons-material/FmdGoodOutlined'
import AttachMoneyOutlinedIcon from '@mui/icons-material/AttachMoneyOutlined'

export default function Details({ event }: { event: Event }) {
  const t = useTranslations('Event')

  return (
    <Paper
      sx={(theme) => ({
        boxShadow: theme.customShadows[1],
        border: 'none',
        padding: 3
      })}
    >
      <ListItem
        icon={<CalendarTodayOutlinedIcon />}
        title={t('Date')}
        subheader={<DisplayDate date={event.datetime_utc} options={{ dateStyle: 'short', timeStyle: 'short' }} />}
      />
      <Divider sx={{ my: 1.5 }} />
      <ListItem
        icon={<FmdGoodOutlinedIcon />}
        title={t('Address')}
        subheader={`${event.venue.city}, ${event.venue.state}, ${event.venue.address} `}
      />
      {event.stats.lowest_price !== null && (
        <>
          <Divider sx={{ my: 1.5 }} />
          <ListItem
            icon={<AttachMoneyOutlinedIcon />}
            title={t('StartingPrice')}
            subheader={<DisplayCurrency number={event.stats.lowest_price} />}
          />
        </>
      )}
    </Paper>
  )
}
