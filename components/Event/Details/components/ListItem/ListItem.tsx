import { Box, Stack, Typography } from '@mui/material'
import { ReactNode } from 'react'

export default function ListItem({
  icon,
  title,
  subheader
}: {
  icon: ReactNode
  title: ReactNode
  subheader: ReactNode
}) {
  return (
    <Stack>
      <Box display='flex' alignItems='center' gap={2}>
        <Box maxWidth='24px' width='100%' display='flex' alignItems='center'>
          {icon}
        </Box>
        <Stack>
          <Typography variant='subtitle2' lineHeight='16px'>
            {title}
          </Typography>
          <Typography variant='body2' color='textSecondary'>
            {subheader}
          </Typography>
        </Stack>
      </Box>
    </Stack>
  )
}
