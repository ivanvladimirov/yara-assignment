import { CreateEventValuesType } from '@/types/event'
import { DateTime } from 'luxon'
import { useTranslations } from 'next-intl'
import { useRouter } from '@/navigation'
import { enqueueSnackbar } from 'notistack'
import * as yup from 'yup'

export default function useCreateEvent() {
  const t = useTranslations()
  const router = useRouter()

  const initialValues: CreateEventValuesType = {
    short_title: '',
    datetime_utc: DateTime.now().plus({ days: 1 }).startOf('day').toISO() as string,
    performers: [{ name: '', image: null, taxonomies: [] }],
    stats: {
      lowest_price: '0'
    },
    venue: {
      address: '',
      city: '',
      extended_address: '',
      name: '',
      state: ''
    },
    taxonomies: []
  }

  const validationSchema = yup.object().shape({
    short_title: yup.string().required(t('Validations.Required')),
    datetime_utc: yup.date().required(t('Validations.Required')),
    performers: yup.array().of(
      yup.object().shape({
        name: yup.string().required(t('Validations.Required'))
      })
    ),
    stats: yup.object().shape({
      lowest_price: yup
        .number()
        .min(0, t('Validations.BelowMin', { min: 0 }))
        .required(t('Validations.Required'))
    }),
    venue: yup.object().shape({
      address: yup.string().required(t('Validations.Required')),
      city: yup.string().required(t('Validations.Required')),
      extended_address: yup.string(),
      name: yup.string().required(t('Validations.Required')),
      state: yup.string().required(t('Validations.Required'))
    }),
    taxonomies: yup.array().min(1).required(t('Validations.Required'))
  })

  const handleSubmit = (values: CreateEventValuesType) => {
    const extended_address = `${values.venue.city}, ${values.venue.state}`
    const taxonomies = values.taxonomies.map((taxonomy) => ({ name: taxonomy, id: taxonomy }))
    const previouslyCreatedEvents = !!localStorage.getItem('my-events')
      ? JSON.parse(localStorage.getItem('my-events') as string)
      : null
    const id = !!previouslyCreatedEvents ? previouslyCreatedEvents.length + 1 : 1

    const event = {
      id,
      ...values,
      venue: { ...values.venue, extended_address },
      performers: values.performers.map((performer) => ({ ...performer, taxonomies })),
      taxonomies
    }

    localStorage.setItem(
      'my-events',
      !!previouslyCreatedEvents ? JSON.stringify([...previouslyCreatedEvents, event]) : JSON.stringify([event])
    )
    enqueueSnackbar(t('Event.EventCreated'), { variant: 'success' })

    router.push(`/event/create/${id}`)
  }

  return { initialValues, validationSchema, handleSubmit }
}
