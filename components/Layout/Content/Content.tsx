'use client'
import { ReactNode } from 'react'
import { Box } from '@mui/material'

export default function Content({ children }: { children: ReactNode }) {
  return (
    <Box component='main' sx={(theme) => ({ mt: `${theme.sizing.header.height}px`, flexGrow: 1 })}>
      {children}
    </Box>
  )
}
