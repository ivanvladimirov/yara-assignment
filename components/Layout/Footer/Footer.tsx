'use client'
import { Box, Button, Container, Grid, Stack, TextField, Typography, Link, LinkProps, Divider } from '@mui/material'
import { ReactNode } from 'react'
import { useTranslations } from 'next-intl'
import Image from 'next/image'
import { Logo } from '@/components/Miscellaneous'
import { DateTime } from 'luxon'

export default function Footer() {
  const t = useTranslations('Footer')

  return (
    <Box
      component='footer'
      sx={{ border: 0, borderTop: '1px solid', borderColor: 'divider', borderStyle: 'dashed', mt: 8 }}
    >
      <Container sx={{ py: 8 }}>
        <Grid container spacing={3} justifyContent='space-between'>
          <Grid item xs={12} md={4}>
            <Typography variant='h6'>{t('StayInTouch')}</Typography>
            <Typography variant='caption' color='textSecondary'>
              {t('SubscribeToNewsletter')}
            </Typography>
            <Box
              component='form'
              onSubmit={(e) => e.preventDefault()}
              display='flex'
              alignItems='center'
              sx={{
                borderRadius: 2,
                bgcolor: 'grey.200',
                px: 1,
                mt: 1,
                transition: 'background 0.2s',
                '&:hover': {
                  transition: 'background 0.2s',
                  bgcolor: 'grey.300'
                }
              }}
            >
              <TextField
                sx={{ p: 1 }}
                placeholder={t('Email')}
                fullWidth
                variant='standard'
                type='email'
                InputProps={{ disableUnderline: true }}
              />
              <Button
                variant='contained'
                sx={{
                  backgroundColor: 'text.primary',
                  color: 'common.white',
                  '&:hover': {
                    backgroundColor: 'text.secondary'
                  }
                }}
              >
                <Typography variant='body2' noWrap sx={{ minWidth: 120 }}>
                  {t('Subscribe')}
                </Typography>
              </Button>
            </Box>
            <Box mt={4} display='flex' gap={2} flexWrap='wrap' sx={{ width: '100%' }}>
              <Image
                alt='Get on App Store'
                src='/images/appstore.png'
                width={142}
                height={48}
                style={{ cursor: 'pointer' }}
              />
              <Image
                alt='Get on Google Play'
                src='/images/googleplay.png'
                width={162}
                height={48}
                style={{ cursor: 'pointer' }}
              />
            </Box>
          </Grid>
          <Grid
            item
            xs={12}
            md={8}
            justifyContent={{ xs: 'space-between', md: 'end' }}
            display='flex'
            flexWrap='wrap'
            sx={{ gap: { xs: 4, sm: 10, md: 16 } }}
          >
            <Stack>
              <Typography variant='h6' gutterBottom>
                {t('Resources')}
              </Typography>
              <CustomLink href='#'>{t('About')}</CustomLink>
              <CustomLink href='#'>{t('Press')}</CustomLink>
              <CustomLink href='#'>{t('Jobs')}</CustomLink>
              <CustomLink href='#'>{t('Inclusion')}</CustomLink>
              <CustomLink href='#'>{t('Blog')}</CustomLink>
              <CustomLink href='#'>{t('Help')}</CustomLink>
              <CustomLink href='#'>{t('Enterprise')}</CustomLink>
              <CustomLink href='#'>{t('Creators')}</CustomLink>
            </Stack>
            <Stack>
              <Typography variant='h6' gutterBottom>
                Social
              </Typography>
              <CustomLink href='#'>Twitter</CustomLink>
              <CustomLink href='#'>Facebook</CustomLink>
              <CustomLink href='#'>Instagram</CustomLink>
            </Stack>
            <Stack>
              <Typography variant='h6' gutterBottom>
                {t('Developers')}
              </Typography>
              <CustomLink href='#'>{t('Platform')}</CustomLink>
              <CustomLink href='#'>{t('Developers')}</CustomLink>
            </Stack>
          </Grid>
        </Grid>
      </Container>
      <Divider />
      <Container
        sx={{ py: 4, display: 'flex', alignItems: 'center', justifyContent: 'space-between', flexWrap: 'wrap' }}
      >
        <Box display='flex' alignItems='center'>
          <Logo />
          <Typography ml={2} variant='body1' color='textSecondary'>
            © {DateTime.now().year}. {t('Rights')}
          </Typography>
        </Box>
        <Box display='flex' gap={4} alignItems='center'>
          <Link href='#' color='textSecondary' sx={{ mb: 0 }}>
            {t('Terms')}
          </Link>
          <Link href='#' color='textSecondary' sx={{ mb: 0 }}>
            {t('Privacy')}
          </Link>
          <Link href='#' color='textSecondary' sx={{ mb: 0 }}>
            {t('Sitemap')}
          </Link>
        </Box>
      </Container>
    </Box>
  )
}

interface CustomLinkType extends LinkProps {
  children: ReactNode
}

const CustomLink = ({ children, ...rest }: CustomLinkType) => {
  return (
    <Link color='textSecondary' sx={{ cursor: 'pointer', mb: 1 }}>
      {children}
    </Link>
  )
}
