import { Button } from '@mui/material'
import MenuIcon from '@mui/icons-material/Menu'

export default function BurgerMenuButton({
  toggleDrawer
}: {
  toggleDrawer: (open: boolean) => (event: React.KeyboardEvent | React.MouseEvent) => void
}) {
  return (
    <Button
      sx={{
        display: { xs: 'inline-flex', md: 'none' },
        ml: 1,
        minWidth: 36,
        backgroundColor: 'text.primary',
        color: 'common.white',
        '&:hover': {
          backgroundColor: 'text.secondary'
        }
      }}
      onClick={toggleDrawer(true)}
    >
      <MenuIcon />
    </Button>
  )
}
