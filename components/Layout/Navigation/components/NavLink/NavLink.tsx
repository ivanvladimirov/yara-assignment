import { locales } from '@/app-config'
import { Button, ButtonProps } from '@mui/material'
import { Link } from '@/navigation'
import { usePathname } from 'next/navigation'
import { ReactNode } from 'react'

interface NavLinkProps extends ButtonProps {
  children: ReactNode
  href: string
  trigger: boolean
}

export default function NavLink({ children, href, trigger, ...rest }: NavLinkProps) {
  const pathname = usePathname()
  const isHomepage = locales.map((locale) => `/${locale}`).includes(pathname)

  return (
    <Button
      component={Link}
      href={href}
      sx={{ color: trigger && isHomepage ? 'text.primary' : !isHomepage ? 'text.primary' : 'common.white', mx: 1.5 }}
      disableRipple
      {...rest}
    >
      {children}
    </Button>
  )
}
