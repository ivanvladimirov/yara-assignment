import { Box, Button, IconButton, List, ListItem, ListItemText, Popover, Tooltip, Typography } from '@mui/material'
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined'
import { useEffect, useRef, useState } from 'react'
import { MAX_TICKETS_TO_BUY } from '@/app-config'
import RemoveIcon from '@mui/icons-material/Remove'
import AddIcon from '@mui/icons-material/Add'
import { useCart } from '@/contexts'
import { DisplayCurrency } from '@/components/Miscellaneous'
import { useTranslations } from 'next-intl'

export default function Cart() {
  const t = useTranslations('Cart')
  const { cart, openCart, total, addToCart, removeFromCart, setOpenCart } = useCart()
  const buttonRef = useRef(null)

  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null)

  useEffect(() => {
    setAnchorEl(buttonRef.current)
  }, [setAnchorEl])

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setOpenCart(true)
  }

  const handleClose = () => {
    setOpenCart(false)
  }

  const open = Boolean(openCart)

  return (
    <>
      <Button
        ref={buttonRef}
        sx={{
          ml: 1,
          minWidth: 36,
          backgroundColor: 'text.primary',
          color: 'common.white',
          '&:hover': {
            backgroundColor: 'text.secondary'
          }
        }}
        onClick={handleClick}
      >
        <ShoppingCartOutlinedIcon />
      </Button>
      <Popover
        slotProps={{ paper: { sx: { padding: 0 } } }}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right'
        }}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
      >
        {!!cart.length ? (
          <>
            <List>
              {cart.map((item) => (
                <ListItem
                  key={item.id}
                  sx={{ minWidth: 300, pr: 14 }}
                  secondaryAction={
                    <Box display='flex' alignItems='center'>
                      <Tooltip
                        title={
                          <>
                            -<DisplayCurrency number={item.price} />
                          </>
                        }
                      >
                        <IconButton size='small' onClick={() => removeFromCart(item.id)}>
                          <RemoveIcon />
                        </IconButton>
                      </Tooltip>
                      <Typography mx={1} sx={{ cursor: 'default', fontSize: '1rem' }}>
                        {item.quantity}
                      </Typography>
                      <Tooltip
                        title={
                          <>
                            +<DisplayCurrency number={item.price} />
                          </>
                        }
                      >
                        <IconButton
                          size='small'
                          disabled={item.quantity >= MAX_TICKETS_TO_BUY}
                          onClick={() => addToCart({ ...item, quantity: 1 })}
                        >
                          <AddIcon />
                        </IconButton>
                      </Tooltip>
                    </Box>
                  }
                >
                  <ListItemText
                    sx={{ maxWidth: 200 }}
                    primary={<Typography noWrap>{item.name}</Typography>}
                    secondary={<DisplayCurrency number={item.price * item.quantity} />}
                  />
                </ListItem>
              ))}
              <ListItem sx={{ display: 'flex', justifyContent: 'space-between' }}>
                <Typography fontWeight={500}>{t('Total')}</Typography>
                <Typography>
                  <DisplayCurrency number={total} />
                </Typography>
              </ListItem>
            </List>
            <Box display='flex' justifyContent='end' sx={{ width: '100%', px: 2, pb: 2 }}>
              <Button variant='contained' color='primary' fullWidth>
                {t('BuyTickets')}
              </Button>
            </Box>
          </>
        ) : (
          <Typography sx={{ p: 2 }} align='center'>
            {t('NoItems')}
          </Typography>
        )}
      </Popover>
    </>
  )
}
