'use client'
import LanguageIcon from '@mui/icons-material/Language'
import { Button, Popover, List, ListItem, ListItemButton } from '@mui/material'
import { useLocale } from 'next-intl'
import { useState, MouseEvent } from 'react'
import { useRouter } from 'next/navigation'
import { usePathname } from '@/navigation'

export default function LanguageSwitcher() {
  const pathname = usePathname()
  const router = useRouter()
  const locale = useLocale()

  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null)

  const handleClick = (event: MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const open = Boolean(anchorEl)
  return (
    <>
      <Button
        sx={{
          minWidth: 36,
          backgroundColor: 'text.primary',
          color: 'common.white',
          '&:hover': {
            backgroundColor: 'text.secondary'
          }
        }}
        onClick={handleClick}
      >
        <LanguageIcon />
      </Button>
      <Popover
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right'
        }}
      >
        <List sx={{ p: 0 }}>
          <ListItem sx={{ px: 0, pb: 0, fontWeight: locale === 'bg' ? 600 : 400 }}>
            <ListItemButton onClick={() => router.push(`/bg/${pathname}`)}>Български</ListItemButton>
          </ListItem>
          <ListItem sx={{ px: 0, pt: 0, fontWeight: locale === 'en' ? 600 : 400 }}>
            <ListItemButton onClick={() => router.push(`/en/${pathname}`)}>English</ListItemButton>
          </ListItem>
        </List>
      </Popover>
    </>
  )
}
