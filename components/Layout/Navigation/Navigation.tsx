'use client'
import { Logo } from '@/components/Miscellaneous'
import {
  AppBar,
  Container,
  Box,
  Button,
  alpha,
  useScrollTrigger,
  Drawer,
  List,
  ListItem,
  ListItemButton,
  Stack,
  useMediaQuery,
  useTheme
} from '@mui/material'
import { NavLink, Cart, BurgerMenuButton, LanguageSwitcher } from './components'
import { useTranslations } from 'next-intl'
import { Link } from '@/navigation'
import { locales } from '@/app-config'
import { usePathname } from 'next/navigation'
import { useState } from 'react'

export default function Navigation() {
  const pathname = usePathname()
  const theme = useTheme()
  const isMobile = useMediaQuery(theme.breakpoints.down('md'))
  const trigger = useScrollTrigger({ disableHysteresis: true, threshold: 10 })
  const t = useTranslations('Navigation')
  const isHomepage = locales.map((locale) => `/${locale}`).includes(pathname)

  const [openDrawer, setOpenDrawer] = useState(false)

  const toggleDrawer = (open: boolean) => (event: React.KeyboardEvent | React.MouseEvent) => {
    if (
      event.type === 'keydown' &&
      ((event as React.KeyboardEvent).key === 'Tab' || (event as React.KeyboardEvent).key === 'Shift')
    ) {
      return
    }

    setOpenDrawer(open)
  }

  const nav = [
    { label: t('Home'), href: '/' },
    { label: t('Search'), href: '/search' },
    { label: t('Wishlist'), href: '/wishlist' }
  ]

  return (
    <AppBar
      color='transparent'
      position='fixed'
      elevation={0}
      sx={(theme) => ({
        borderRadius: 0,
        border: 'none',
        background: isHomepage
          ? trigger
            ? alpha(theme.palette.common.white, 0.8)
            : 'transparent'
          : alpha(theme.palette.common.white, 0.8),
        height: trigger ? theme.sizing.header.collapsedHeight : theme.sizing.header.height,
        color: theme.palette.text.primary,
        justifyContent: 'center',
        backdropFilter: trigger ? 'blur(6px)' : 'none',
        transition: 'height 0.12s'
      })}
    >
      <Container sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
        <Logo isWhite={isHomepage && !trigger} />
        <Box display={{ xs: 'none', md: 'flex' }}>
          {nav.map((item) => (
            <NavLink key={item.label} href={item.href} trigger={trigger}>
              {item.label}
            </NavLink>
          ))}
        </Box>
        <Box display='flex' alignItems='center'>
          <LanguageSwitcher />
          <Button
            component={Link}
            href='/event/create'
            variant='contained'
            color='inherit'
            sx={{
              ml: 1,
              maxHeight: 36,
              display: { xs: 'none', md: 'initial' },
              backgroundColor: 'text.primary',
              color: 'common.white',
              '&:hover': {
                backgroundColor: 'text.secondary'
              }
            }}
          >
            {t('CreateEvent')}
          </Button>
          <Cart />
          <BurgerMenuButton toggleDrawer={toggleDrawer} />
        </Box>
      </Container>
      <Drawer
        anchor='left'
        open={isMobile ? openDrawer : false}
        onClose={toggleDrawer(false)}
        PaperProps={{ sx: { borderRadius: 0 } }}
      >
        <Stack minWidth={250}>
          <Box
            sx={(theme) => ({
              display: 'flex',
              alignItems: 'center',
              borderBottom: '1px dashed',
              borderColor: 'divider',
              height: trigger ? theme.sizing.header.collapsedHeight : theme.sizing.header.height
            })}
          >
            <Container sx={{ display: 'flex', alignItems: 'center' }}>
              <Logo />
            </Container>
          </Box>
          <List>
            {nav.map((item) => (
              <ListItem key={item.href} disablePadding>
                <ListItemButton component={Link} href={item.href} onClick={toggleDrawer(false)}>
                  {item.label}
                </ListItemButton>
              </ListItem>
            ))}
            <ListItem disablePadding>
              <ListItemButton component={Link} href='/event/create' onClick={toggleDrawer(false)}>
                {t('CreateEvent')}
              </ListItemButton>
            </ListItem>
          </List>
        </Stack>
      </Drawer>
    </AppBar>
  )
}
