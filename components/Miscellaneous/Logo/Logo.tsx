import Image from 'next/image'

export default function Logo({
  isWhite = false,
  width = 77,
  height = 30
}: {
  isWhite?: boolean
  width?: number
  height?: number
}) {
  const file = isWhite ? 'logo-white' : 'logo-black'

  return <Image alt='logo' width={width} height={height} src={`/images/${file}.png`} />
}
