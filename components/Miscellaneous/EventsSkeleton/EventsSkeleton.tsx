import EventSuspense from '@/components/Miscellaneous/Event/EventSuspense'
import { Grid, GridProps } from '@mui/material'

interface EventsSkeletonInterface extends GridProps {
  count: number
}

export default function EventsSkeleton({ count, ...rest }: EventsSkeletonInterface) {
  return (
    <Grid container spacing={3} {...rest}>
      {Array(count)
        .fill(undefined)
        .map((item, index) => (
          <Grid key={index} item xs={6} sm={4} md={3}>
            <EventSuspense />
          </Grid>
        ))}
    </Grid>
  )
}
