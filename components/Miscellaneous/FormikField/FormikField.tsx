'use client'
import { TextField, TextFieldProps } from '@mui/material'
import { FormikValues, useFormikContext } from 'formik'
import { ReactNode } from 'react'

function getNestedValue(obj: any, path: string) {
  return path.split('.').reduce((acc, key) => {
    if (acc && key.includes('[')) {
      const [arrayKey, index] = key.split(/[\[\]]/).filter(Boolean)
      return acc[arrayKey] && acc[arrayKey][index] !== undefined ? acc[arrayKey][index] : undefined
    }
    return acc && acc[key] !== undefined ? acc[key] : undefined
  }, obj)
}

function setNestedValue(obj: any, path: string, value: any) {
  const pathArray = path.split('.')
  const lastKey = pathArray.pop()
  const nestedObj = pathArray.reduce((acc, key) => {
    if (key.includes('[')) {
      const [arrayKey, index] = key.split(/[\[\]]/).filter(Boolean)
      if (!acc[arrayKey]) {
        acc[arrayKey] = []
      }
      if (!acc[arrayKey][index]) {
        acc[arrayKey][index] = {}
      }
      return acc[arrayKey][index]
    }
    if (!acc[key]) {
      acc[key] = {}
    }
    return acc[key]
  }, obj)

  if (lastKey) {
    nestedObj[lastKey] = value
  }
}

export default function FormikField({
  name,
  children,
  ...rest
}: {
  name: string
  [key: string]: TextFieldProps | ReactNode | { [key: string]: ReactNode }
}) {
  const { values, handleBlur, handleChange, touched, errors } = useFormikContext<FormikValues>()

  const value = getNestedValue(values, name)
  const error = getNestedValue(errors, name)
  const touch = getNestedValue(touched, name)

  const defaultProps = {
    name,
    value,
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => {
      const newValue = event.target.value
      setNestedValue(values, name, newValue)
      handleChange(event)
    },
    fullWidth: true,
    onBlur: handleBlur,
    error: touch && Boolean(error),
    helperText: touch && error ? String(error) : ''
  }

  return children ? (
    <TextField {...defaultProps} {...rest}>
      {children as ReactNode}
    </TextField>
  ) : (
    <TextField {...defaultProps} {...rest} />
  )
}
