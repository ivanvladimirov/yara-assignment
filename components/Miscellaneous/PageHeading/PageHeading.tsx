import { Typography, TypographyProps } from '@mui/material'
import { ReactNode } from 'react'

interface PageHeadingType extends TypographyProps {
  children: ReactNode
}

export default function PageHeading({ children, ...rest }: PageHeadingType) {
  return (
    <Typography variant='h5' {...rest}>
      {children}
    </Typography>
  )
}
