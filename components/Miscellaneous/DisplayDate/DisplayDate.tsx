'use client'
import { DateTime } from 'luxon'
import { useParams } from 'next/navigation'

export default function DisplayCurrency({ date, options }: { date: string; options: Intl.DateTimeFormatOptions }) {
  const { locale } = useParams()

  return (
    <span>
      {DateTime.fromISO(date)
        .setLocale(locale as string)
        .toLocaleString(options)}
    </span>
  )
}
