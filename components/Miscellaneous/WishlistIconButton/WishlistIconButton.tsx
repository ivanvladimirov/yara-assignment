'use client'
import { useWishlist } from '@/contexts'
import Favorite from '@mui/icons-material/Favorite'
import { IconButton, IconButtonProps, SvgIconOwnProps } from '@mui/material'

interface WishlistIconButtonType extends IconButtonProps {
  eventId: number
  fontSize?: SvgIconOwnProps['fontSize']
}

export default function WishlistIconButton({ eventId, fontSize = 'medium', ...props }: WishlistIconButtonType) {
  const { loadingWishlist, updateWishlist, wishlist } = useWishlist()

  return (
    <IconButton
      disabled={loadingWishlist}
      onClick={() => {
        eventId && updateWishlist(eventId)
      }}
      {...props}
    >
      <Favorite
        sx={(theme) => {
          return {
            strokeWidth: '2px',
            stroke: theme.palette.common.white,
            fill: wishlist.includes(eventId) ? theme.palette.primary.main : theme.palette.common.black
          }
        }}
        fontSize={fontSize}
      />
    </IconButton>
  )
}
