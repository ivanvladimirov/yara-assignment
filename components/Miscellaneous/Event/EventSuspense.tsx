import { Stack, Typography, Box, Skeleton } from '@mui/material'

export default function EventSuspense() {
  return (
    <Stack maxWidth={584} width={'100%'}>
      <Box
        sx={{
          overflow: 'hidden',
          borderRadius: 2,
          position: 'relative',
          paddingTop: { md: '59.8%', sm: '76%', xs: '104%' }
        }}
      >
        <Skeleton
          sx={{
            height: '500px',
            width: '100%',
            position: 'absolute',
            transform: 'translateY(-29.9%)',
            top: 0,
            left: 0
          }}
        />
      </Box>
      <Typography variant='h6' sx={{ mt: 2 }}>
        <Skeleton width={'90%'} />
      </Typography>
      <Typography variant='body1' sx={{ mb: 1 }}>
        <Skeleton width={'70%'} />
      </Typography>
      <Typography variant='body1'>
        <Skeleton width={'40%'} />
      </Typography>
    </Stack>
  )
}
