'use client'
import { Event } from '@/types/event'
import { CardMedia, Stack, Typography, Box, Tooltip } from '@mui/material'
import { DisplayCurrency, DisplayDate, WishlistIconButton } from '@/components/Miscellaneous'
import { useTranslations } from 'next-intl'
import { useRouter } from '@/navigation'
import Image from 'next/image'

export default function Event({ event }: { event: Event }) {
  const t = useTranslations('Home')
  const router = useRouter()

  return (
    <Stack maxWidth={584} width={'100%'} sx={{ cursor: 'pointer' }}>
      <Box sx={{ overflow: 'hidden', borderRadius: 2, position: 'relative' }}>
        <WishlistIconButton eventId={event.id} sx={{ position: 'absolute', top: '4px', right: '4px', zIndex: 1 }} />
        <Box
          onClick={() => router.push(`/event/${event.id}`)}
          sx={{
            position: 'relative',
            paddingTop: { md: '59.8%', sm: '76%', xs: '104%' },
            overflow: 'hidden',
            transitionDuration: '0.15s',
            '& img:hover': {
              transform: 'scale(1.06)',
              transitionDuration: '0.2s'
            }
          }}
        >
          <Image
            alt={event.performers[0].name}
            fill
            style={{
              objectFit: 'cover',
              transitionDuration: '0.15s'
            }}
            src={event.performers[0].image as string}
            quality={100}
          />
        </Box>
      </Box>
      <Tooltip title={event.title} placement='bottom-start'>
        <Typography variant='h6' sx={{ mt: 2 }} noWrap onClick={() => router.push(`/event/${event.id}`)}>
          {event.performers[0].name}
        </Typography>
      </Tooltip>
      <Typography
        variant='body1'
        color='textSecondary'
        sx={{ mb: 1 }}
        noWrap
        onClick={() => router.push(`/event/${event.id}`)}
      >
        <DisplayDate date={event.datetime_utc} options={{ day: 'numeric', month: 'short' }} /> · {event.venue.address}
      </Typography>
      <Typography variant='body1' onClick={() => router.push(`/event/${event.id}`)}>
        {t('From')} <DisplayCurrency number={event.stats.lowest_price} />
      </Typography>
    </Stack>
  )
}
