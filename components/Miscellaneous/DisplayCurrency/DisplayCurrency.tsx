'use client'
import { DEFAULT_CURRENCY } from '@/app-config'
import { useParams } from 'next/navigation'

export default function DisplayCurrency({ number }: { number: number }) {
  const { locale } = useParams()

  return <span>{Intl.NumberFormat(locale, { style: 'currency', currency: DEFAULT_CURRENCY }).format(number)}</span>
}
