import { Locale } from './types/locale'

export const DEFAULT_CURRENCY = 'USD'
export const DEFAULT_LOCALE: Locale = 'bg'
export const locales: Locale[] = [DEFAULT_LOCALE, 'en']
export const PREDEFINED_CATEGORIES: { [key: string]: string[] } = {
  'concert-events': ['concert'],
  'sport-events': ['sports', 'mls', 'nba', 'nfl', 'mlb', 'fighting', 'wwe', 'nascar', 'tennis', 'golf'],
  'theater-events': ['theater', 'comedy', 'broadway_tickets_national', 'family']
}
export const MAX_TICKETS_TO_BUY = 5
