import { locales } from '@/app-config'
import { createSharedPathnamesNavigation } from 'next-intl/navigation'

export const { Link, redirect, usePathname, useRouter } = createSharedPathnamesNavigation({ locales })
