const objectToQueryString = (obj: Record<string, string | number | string[] | number[]>) => {
  return Object.keys(obj)
    .map((key) => {
      const isArray = Array.isArray(obj[key])

      if (isArray) {
        return (
          obj[key]
            // @ts-ignore
            .map(
              (_key: string | number) =>
                `${encodeURIComponent(key)}=${encodeURIComponent(typeof _key === 'string' ? _key.trim() : _key)}`
            )
            .join('&')
        )
      } else {
        return `${encodeURIComponent(key)}=${encodeURIComponent(
          // @ts-ignore
          typeof obj[key] === 'string' ? obj[key]?.trim() : obj[key]
        )}`
      }
    })
    .join('&')
}

export default objectToQueryString
