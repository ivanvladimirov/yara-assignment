import { EventQueryParams, FetchEventsResponse } from '@/types/event'
import objectToQueryString from './objectToQueryString'

export default async function fetchPerformers(props: Partial<EventQueryParams> = {}): Promise<FetchEventsResponse> {
  const res = await fetch(
    `https://api.seatgeek.com/2/performers?client_id=${
      process.env.NEXT_PUBLIC_SEATGEEK_PUBLIC_KEY
    }&${objectToQueryString(props)}`
  )
  if (!res.ok) {
    throw new Error('Failed to fetch performers')
  }

  return res.json()
}
