import { EventQueryParams, FetchEventsResponse } from '@/types/event'
import objectToQueryString from './objectToQueryString'

export default async function fetchEvents(
  props: Partial<EventQueryParams> = {},
  errorMessage?: string
): Promise<FetchEventsResponse> {
  const res = await fetch(
    `https://api.seatgeek.com/2/events?client_id=${process.env.NEXT_PUBLIC_SEATGEEK_PUBLIC_KEY}&${objectToQueryString(
      props
    )}`,
    { cache: 'no-cache' }
  )

  if (!res.ok) {
    throw new Error(!!errorMessage ? errorMessage : 'Failed to fetch events')
  }

  return res.json()
}
